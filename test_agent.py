#!/usr/bin/env python3

"""
Dummy agent that loads some trained neural network and performs actions
with it to test if the training was successful.

"""

import argparse
import importlib
import logging
from pathlib import Path
import os
import random

import numpy as np
import torch

import env_settings
import environment
from to_csv import Store


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def run_test(nn_path: str, rainy_type=None):
    settings_path = os.path.dirname(nn_path) + '/env_settings.json'
    settings = env_settings.load_settings_from_json(settings_path)
    settings['seed'] = 1
    env = environment.QMarketEnv(**settings)

    env.prevent_overflow = True
    env.prevent_underflow = True
    env.action_penalty_factor = 0

    agent = TestAgent(env, nn_path, rainy_type)

    data_file = nn_path + '_evaluation.csv'
    store = Store(filename=data_file)

    logger.info(f'Test agent: {nn_path}')
    scores = []
    # Go through all predefined test episodes
    for idx in range(len(env.test_episodes)):
        logger.info(f'Test episode {idx}')
        # TODO: Make sure these resets are deterministic!!! Random seed!
        observation = env.test_reset(test_episode=idx)
        done = False
        score = 0
        counter = 0
        while not done:
            counter += 1
            action = agent.choose_action(observation)
            observation, reward, done, _ = env.step(action)

            score += reward

            if data_file:
                data = env.get_health_metrics(verbose=True)
                data['reward'] = reward
                data['step'] = env.current_step
                data.update({
                    f'obs{i}: ' + str(env.observation_space_keys[i%len(env.observation_space_keys)]): obs
                             for i, obs in enumerate(env.current_obs)})
                data.update({f'act{i}: ' + str(env.action_keys[i]): act
                             for i, act in enumerate(env.true_actions)})
                data.update({f'agent_act{i}: ' + str(env.action_keys[i]): act
                             for i, act in enumerate(env.original_actions)})
                store.to_csv(data)

        score = score/counter*24  # average daily score
        scores.append((f'Episode {idx}', score))

        # TODO: Maybe evaluate performance compared to baseline directly?!

    logger.info(f'Scores: {scores}')
    return data_file


class TestAgent():
    def __init__(self, env, nn_path: str, rainy_type: str=''):
        # TODO: Load model first, if only weight dict is given
        if isinstance(rainy_type, str) and rainy_type:
            self.set_rainy_agent(env, nn_path, 'run_' + rainy_type)
        else:
            # For custom DDPG
            self.model = torch.load(nn_path)
            self.model.eval()

    def choose_action(self, obs: "numpy.array") -> "numpy.array":
        state = torch.tensor(obs, dtype=torch.float)
        actions = self.model.forward(state)
        return actions.detach().numpy()

    def set_rainy_agent(self, env, nn_path, rainy_type):
        module = importlib.import_module(rainy_type)
        self.exp = getattr(module, rainy_type)(env=env, settings={'seed': env._seed}, path=None, get_model_for_test=True)
        # self.exp.load_and_evaluate(nn_path)
        success = self.exp._load_agent(Path(nn_path))
        if not success:
            logger.critical('Experiment could not be loaded for unknown reasons')
            exit()
        logger.debug(f'Successfully loaded agent: {success}')
        self.exp.ag.set_mode(train=False)
        self.choose_action = (lambda
            s: np.clip(self.exp.ag.eval_action(s), 0, 1))



def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--nn-path',
        default='data/DDPG_Results/test.pth',
        help="Name of pth file where the trained neural net is stored")
    argparser.add_argument(
        '--rainy-type',
        default='',
        help="Type of used rainy agent. Normally just drawn from filename.")
    args = argparser.parse_args()

    if not args.rainy_type:
        args.rainy_type = args.nn_path.split('/')[-1].split('_')[0]

    run_test(args.nn_path, rainy_type=args.rainy_type)


if __name__ == '__main__':
    main()
