# to_csv.py
"""
Store some data in simple csv file.

TODO: terribly written code

"""

import csv

class Store():
    def __init__(self, filename: str):
        self.filename = filename
        self.is_new = True

    def to_csv(self, data: dict):
        if self.is_new:
            self.fieldnames = sorted(list(data.keys()))

        self._dict_to_csv(row=data)

    def _dict_to_csv(self, row: dict):
        with open(self.filename, 'a') as f:
            writer = csv.DictWriter(f, self.fieldnames, delimiter=',')
            if self.is_new:
                writer.writeheader()
                self.is_new = False
            writer.writerow(row)
