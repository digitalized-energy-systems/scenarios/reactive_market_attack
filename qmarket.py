# qmarket.py
"""
Create a simple reactive power market for the e-energy 2021 paper.

"""

import math

import numpy as np
import pandapower as pp
import simbench as sb


def define_qmarket(grid_id: str, p_price_euro_per_mwh: float, scaling_loads=1.0, scaling_gens=1.0, p_gen_controllable=False, q_price_euro_per_mvarh2=250, profit_factor_q=2):
    # https://simbench.de/wp-content/uploads/2020/01/simbench_documentation_de_1.0.1.pdf
    net = sb.get_simbench_net(grid_id)
    _scale_units(net, scaling_loads, scaling_gens)
    profiles = _init_timeseries(net)
    _define_system_constraints(net)
    _define_unit_constraints(net, profiles, p_gen_controllable)
    _define_cost_function(net, p_price_euro_per_mwh,
        q_price_euro_per_mvarh2=q_price_euro_per_mvarh2,
        profit_factor_q=profit_factor_q)

    return net, profiles


def _scale_units(net, scaling_loads=1.0, scaling_gens=1.0):
    net.load['scaling'] = scaling_loads
    net.sgen['scaling'] = scaling_gens

def _init_timeseries(net):
    """ Store simbench timeseries for later updating of the net. """
    assert not sb.profiles_are_missing(net)
    profiles = sb.get_absolute_values(net,
        profiles_instead_of_study_cases=True)
    return profiles

def _define_system_constraints(net, u_min=0.95, u_max=1.05,
                               max_loading_line=60, max_loading_trafo=60):
    net.bus['min_vm_pu'] = u_min
    net.bus['max_vm_pu'] = u_max
    net.line['max_loading_percent'] = max_loading_line
    net.trafo['max_loading_percent'] = max_loading_trafo

def _define_unit_constraints(net, profiles, p_gen_controllable=True, cos_phi_gen=0.95, allow_slack_q_flow=True):
    """ Called once in the beginning. """
    net.ext_grid['min_p_mw'] = -np.inf
    net.ext_grid['max_p_mw'] = np.inf
    # No reactive power flow from superordinate grid allowed
    if not allow_slack_q_flow:
        net.ext_grid['min_q_mvar'] = 0
        net.ext_grid['max_q_mvar'] = 0
    net.ext_grid['controllable'] = True

    net.load['min_p_mw_tech'] = 0
    net.load['max_p_mw_tech'] = profiles[('load', 'p_mw')].max(axis=0) * net['load']['scaling']
    net.load['min_q_mvar_tech'] = 0
    net.load['max_q_mvar_tech'] = profiles[('load', 'q_mvar')].max(axis=0) * net['load']['scaling']
    net.load['controllable'] = False

    # Set tap position to prevent voltage violations all the time
    net.trafo['tap_pos'] = 1

    for unit_type in ('gen', 'sgen'):
        net[unit_type]['cos_phi'] = cos_phi_gen
        # Assumption: max technical power of gens is max p from timeseries
        net[unit_type]['p_mw_tech'] = profiles[(unit_type, 'p_mw')].max(axis=0) * net[unit_type]['scaling']
        net[unit_type]['max_s_mva'] = (
            net[unit_type]['p_mw_tech'] / net[unit_type]['cos_phi'])

        # Set absolute technically possible min/max values
        net[unit_type]['min_q_mvar_tech'] = -net[unit_type]['max_s_mva']
        net[unit_type]['max_q_mvar_tech'] = net[unit_type]['max_s_mva']
        net[unit_type]['min_p_mw_tech'] = 0
        net[unit_type]['max_p_mw_tech'] = net[unit_type]['p_mw_tech']

    for unit_type in ('gen', 'sgen'):
        net[unit_type]['controllable'] = True
        net[unit_type]['min_p_mw'] = 0

def update_unit_constraints(net):
    """ Called every step to adjust gen constraints to consider new power
    values. """
    for unit_type in ('gen', 'sgen'):
        # Assume active power from timeseries as max possible value in this
        # time step. (for example due to wind speed or solar radiation)
        net[unit_type]['max_p_mw'] = net[unit_type]['p_mw'] * net[unit_type]['scaling']
        net[unit_type]['min_p_mw'] = net[unit_type]['p_mw'] * net[unit_type]['scaling']

        # Assumption: active power value is fixed
        net[unit_type]['max_q_mvar'] = (
            net[unit_type]['max_s_mva']**2
            - net[unit_type]['max_p_mw']**2)**0.5
        net[unit_type]['min_q_mvar'] = -net[unit_type]['max_q_mvar']

def _define_cost_function(net, p_price_euro_per_mwh=30, profit_factor_p=1, q_price_euro_per_mvarh2=250, profit_factor_q=2):
    """ Cost function for reactive power market: minimize quadratic reactive power costs and linear costs for active power reduction.

    "q_price_euro_per_mvarh2" taken from: 10.1016/j.enconman.2015.09.070
    -> about 0.3 * 10⁻3 $/kvarh² -> 300 $/mvarh² -> 250 €/mvarh²

    These are the costs for the unit operators. The profit factor ensures that they can make profit.

    """
    q_price = q_price_euro_per_mvarh2 * profit_factor_q
    p_price = p_price_euro_per_mwh * profit_factor_p
    # Set prices for all units the same
    for unit_type in ('gen', 'sgen'):
        for element in net[unit_type].index:
            # Negative linear active power price to punish reduction
            # Quadratic reactive power price as ancillary service payment
            pp.create_poly_cost(net, element=element, et=unit_type,
                                cp1_eur_per_mw=-p_price,
                                cq2_eur_per_mvar2=q_price)
    for element in net.ext_grid.index:
        # Quadratic reactive power price as ancillary service payment
        pp.create_poly_cost(net, element=element, et='ext_grid', cp1_eur_per_mw=p_price, cq2_eur_per_mvar2=q_price,
            index=None)


def get_reactive_profits(net, unit_keys: dict):
    profit = 0
    df = net.poly_cost
    for unit_type, idxs in unit_keys.items():
        idxs = list(idxs)
        costs = net.poly_cost.loc[net.poly_cost['element'].isin(idxs)].loc[net.poly_cost.et==unit_type]
        costs = costs.sort_values('element')
        units = net[f'res_{unit_type}'].iloc[idxs]

        assert False not in (costs.element == units.index).values

        profit += sum(costs['cq0_eur'])
        profit += sum(costs['cq1_eur_per_mvar'] * units['q_mvar'])
        profit += sum(costs['cq2_eur_per_mvar2'] * units['q_mvar']**2)

    return profit


if __name__ == '__main__':
    net = define_qmarket(grid_id='1-MV-rural--0-sw', p_price_euro_per_mwh=30)
