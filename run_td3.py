from pathlib import Path
import os

from torch.optim import Adam
import rainy
from rainy.agents import TD3Agent
from rainy.lib import explore
from rainy.experiment import Experiment

from env_settings import store_settings_dict_to_json
import test_agent


def run_td3(env, settings: dict, path: str=None, get_model_for_test=False, autotest=True):
    if not path:
        path = 'data/TD3_Results/Temp'

    c = rainy.Config()
    c.seed = settings['seed']

    env.autoscale_obs = True
    env.use_reward_monitor = False
    c.set_env(lambda: env)

    c.max_steps = int(1e5)
    c.set_optimizer(lambda params: Adam(params, lr=1e-3), key="actor")
    c.set_optimizer(lambda params: Adam(params, lr=1e-3), key="critic")
    c.replay_size = int(1e5)
    c.train_start = int(1e3)
    c.set_explorer(lambda: explore.GaussianNoise())
    c.set_explorer(lambda: explore.Greedy(), key="eval")
    c.set_explorer(
        lambda: explore.GaussianNoise(explore.DummyCooler(0.2), 0.5), key="target"
    )

    c.eval_deterministic = False  # True
    c.eval_freq = None # c.max_steps // 10
    c.save_freq = 10000
    c.grad_clip = None
    c.nworkers = 1
    c.replay_batch_size = 100 * c.nworkers

    c.logger.logdir = Path(path)
    # c.logger.LOG_CAPACITY = 20

    c.action_dim = env.action_space.shape[0]
    c.state_dim = env.observation_space.shape

    agent = TD3Agent(c)

    save_file_name = "td3_model.pth" if path else None
    experiment = Experiment(agent, save_file_name)

    if get_model_for_test:
        return experiment

    experiment.train()

    if autotest:
        test_agent.run_test(nn_path=path+save_file_name, rainy_type='td3')
