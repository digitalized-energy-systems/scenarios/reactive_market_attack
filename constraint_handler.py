# constraint_handler.py
"""
A class to relax pandapower system constraints and reset them to original state afterwards.

"""

import copy


class ConstraintHandler():
    def __init__(self, net, delta_u=0.01, delta_line_loading=10,
                 delta_trafo_loading=10):
        self.store_constraints(net)

        self.counter = 0

        self.delta_u = delta_u
        self.delta_line_loading = delta_line_loading
        self.delta_trafo_loading = delta_trafo_loading

    def store_constraints(self, net):
        """ Safe original network constraints to reset them later. """
        self.u_min = copy.copy(net.bus.min_vm_pu)
        self.u_max = copy.copy(net.bus.max_vm_pu)
        self.max_line_loading = copy.copy(net.line.max_loading_percent)
        self.max_trafo_loading = copy.copy(net.trafo.max_loading_percent)

    def relax_constraints(self, net):
        self.counter += 1
        if self.delta_u is not None:
            net.bus.min_vm_pu -= self.delta_u
            net.bus.max_vm_pu += self.delta_u

        if self.delta_line_loading is not None:
            net.line.max_loading_percent += self.delta_line_loading

        if self.delta_trafo_loading is not None:
            net.trafo.max_loading_percent += self.delta_trafo_loading

    def reset_constraints(self, net):
        """ Set system constraints to original state. """
        self.counter = 0
        net.bus['min_vm_pu'] = self.u_min
        net.bus['max_vm_pu'] = self.u_max
        net.line['max_loading_percent'] = self.max_line_loading
        net.trafo['max_loading_percent'] = self.max_trafo_loading
