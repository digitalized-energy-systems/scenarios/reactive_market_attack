FROM nvcr.io/nvidia/pytorch:20.11-py3
MAINTAINER Eric MSP Veith <veith@offis.de>

ADD . /workspace
RUN mkdir -p /workspace/data
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r /workspace/requirements.txt
RUN find /workspace -type f -exec chmod 0755 {} \; \
    && find /workspace -type d -exec chmod 0755 {} \;

CMD ["/workspace/run.sh"]
