# pp_util.py
""" Utility functions for pandapower. """

import copy

def relax_constraints(net, u_min_delta=0.0, u_max_delta=0.0,
                      line_loading_delta=0, trafo_loading_delta=0):
    """ Relax all system constraints by some delta value to increase probability of successfull OPF calculation. Creates a copy that gets adjusted. """
    net_copy = copy.deepcopy(net)
    net_copy.bus['min_vm_pu'] += u_min_delta
    net_copy.bus['max_vm_pu'] += u_max_delta
    net_copy.line['max_loading_percent'] += line_loading_delta
    net_copy.trafo['max_loading_percent'] += trafo_loading_delta
    return net_copy
