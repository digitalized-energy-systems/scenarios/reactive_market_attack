#!/usr/bin/env python3

import os
import random
import logging
import argparse
import multiprocessing
import multiprocessing.pool
from datetime import datetime

import torch
import numpy as np

import environment
import env_settings
from run_acktr import run_acktr
from run_td3 import run_td3
from run_baseline import exact_baseline


AGENT_TYPES = {
    "baseline": exact_baseline,
    "acktr": run_acktr,
    "td3": run_td3,
}


class NoDaemonProcess(multiprocessing.Process):
    @property
    def daemon(self):
        return False

    @daemon.setter
    def daemon(self, value):
        pass


class NoDaemonContext(type(multiprocessing.get_context())):
    Process = NoDaemonProcess


class NestablePool(multiprocessing.pool.Pool):
    def __init__(self, *args, **kwargs):
        kwargs['context'] = NoDaemonContext()
        super(NestablePool, self).__init__(*args, **kwargs)


def _print(seed, *args, **kwargs):
    print(
        "[PID:%s SEED:%s] " % (os.getpid(), seed),
        *args,
        **kwargs
    )


def generate_seed():
    seed = int.from_bytes(os.urandom(3), byteorder='big')
    print("Seed is: %s" % seed)
    return seed


def seed_rngs(seed):
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)


def run_experiment(name, agent_type, settings: dict, store_results, seed: None):
    if agent_type not in AGENT_TYPES.keys():
        _print(seed, "ERROR: No such agent type '%s', dying." % agent_type)
        return

    if seed is None:
        seed = generate_seed()

    seed_rngs(seed)
    settings["seed"] = seed
    env = environment.QMarketEnv(**settings)
    env.action_space.seed(seed)

    _print(seed, "n inputs: %s" % env.observation_space.shape[0])
    _print(seed, "observation space: %s" % env.observation_space)
    _print(seed, "n actions: %s" % env.action_space.shape[0])
    _print(seed, "action space: %s" % env.action_space)
    _print(seed, "store results: %s" % store_results)
    _print(seed, "agent type: %s" % agent_type)

    if store_results:
        path = create_experiment_folder(name, seed, agent_type, settings)
    else:
        path = None

    AGENT_TYPES[agent_type](env, settings, path)


def create_experiment_folder(name, seed, agent_type, settings):
    # Create path folder
    now = datetime.now().isoformat()
    full_name = '%s_%s_%s_%s' % (now, seed, agent_type, name)
    path = 'data/' + full_name + '/'
    os.mkdir(path)

    # Store some meta-data
    env_settings.store_settings_dict_to_json(settings, path+'env_settings')
    with open(path + 'meta-data.txt', 'w') as f:
        f.write(f'User description: {name}\n')
        f.write(f'Agent type: {agent_type}\n')
        f.write(f'Seed: {seed}\n')

    return path


def main():
    now = datetime.now()

    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--name',
        default='',
        help="Name of the experiment run; should be something descriptive")
    argparser.add_argument(
        '--seed',
        type=int,
        help="Random seed used to reproduce an experiment. Sets pool to 1."
    )
    argparser.add_argument(
        '--num-experiments',
        type=int,
        default=1,
        help="Repeat experiment how many times with different seeds?"
    )
    argparser.add_argument(
        '--pool',
        type=int,
        help="Run as many experiments in parallel. Forced to 1 if --seed is "
             "given",
        default=1
    )
    argparser.add_argument(
        '--agent-type',
        help="Agent type",
        choices=AGENT_TYPES.keys(),
        required=True
    )
    argparser.add_argument(
        '--store-results',
        help="Store all data and plots, if set",
        action='store_true'
    )
    argparser.add_argument(
        '--debug',
        help="Output debbuging information",
        action='store_true',
        default=False
    )
    argparser.add_argument(
        '--experiment-file',
        help="Json-filename where experiment grid for environment is defined. Should be either a dict for a single experiment or a list for multiple experiments with different settings",
        # TODO: If directory is given here, use all files in that directory (but then they should include agent type etc?! Dicts within dicts?)
        type=str,
        default='settings/standard_settings'
    )

    args = argparser.parse_args()
    logging.root.setLevel(logging.DEBUG if args.debug else logging.WARNING)

    pool_size = 1 if args.seed else args.pool

    # TODO: num.experiments gets overwritten here!!! How to deal with num.experiments, if they are explicitly given?
    settings, n_settings = env_settings.user_env_settings_doe(args.experiment_file)

    if pool_size == 1 and args.num_experiments == 1 and n_settings == 1:
        seed = args.seed if args.seed else generate_seed()
        run_experiment(args.name, args.agent_type, settings, args.store_results, seed)
    else:
        if n_settings == 1:
            settings = {'': settings}
        with NestablePool(pool_size) as p:
            args = args.num_experiments * [
                (
                    args.name + setting_name,
                    args.agent_type,
                    setting,
                    args.store_results,
                    args.seed if args.seed else None
                )
                for setting_name, setting in settings.items()
            ]
            p.starmap(run_experiment, args)


if __name__ == '__main__':
    main()