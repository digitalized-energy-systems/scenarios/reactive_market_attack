# env_settings.py

import itertools
import json

#rural: interesting buses 61-68 (mini ring down left): attacking_loads=[56,57,58,59,60,61,62,63,93], attacking_sgens=[56,57,58,59,60,61,62,63,95]
#comm, buses 17-23: attacking_loads=range(7,13), attacking_sgens=range(6,12) (the red overhead lines on the right)

# test_episodes = (
#     (-1, 1*7*24*4),
#     (3500, 3500+1*7*24*4),
#     (7000, 7000+1*7*24*4),
#     (10500, 10500+1*7*24*4),
#     (14000, 14000+1*7*24*4),
#     (17500, 17500+1*7*24*4),
#     (21000, 21000+1*7*24*4),
#     (24500, 24500+1*7*24*4),
#     (28000, 28000+1*7*24*4),
#     (31500, 31500+1*7*24*4)) # 11 test episodes of 1 week ~ 21%


def get_standard_settings():
    # settings = {'grid_id': '1-MV-rural--0-no_sw',
    #         'attacking_loads': [56,57,58,59,60,61,62,63,93],
    #         # buses: (61, 62, 63, 64, 65, 66, 67, 68, 64)
    #         'attacking_sgens': [56,57,58,59,60,61,62,63,95],
    #         # buses: (61, 62, 63, 64, 65, 66, 67, 68, 61)
    #         'train_epis_length': 4*24*4,
    #         'steps_per_storage': 8,
    #         'load_actuators_only': True,  # Adjust here maybe
    #         'prevent_underflow': True,
    #         'prevent_overflow': True,
    #         'scaling_sgens': 1.0,
    #         'scaling_loads': 3.0,
    #         'action_penalty_factor': 5,  # Adjust here?
    #         'test_episodes': test_episodes,
    #         'train_on_test': True}  # Change this, if possible!?
    return load_settings_from_json('settings/standard_settings')


def user_env_settings_doe(
        experiment_filename='settings/multi_standard_settings.json'):
    user_env_settings = load_settings_from_json(experiment_filename)

    if isinstance(user_env_settings, dict):
        # Single experiment case
        settings = get_standard_settings()
        settings.update(user_env_settings)
        return settings, 1

    combinations = [range(len(s[1])) for s in user_env_settings]
    settings = {}
    for positions in itertools.product(*combinations):
        setting_diff, name = get_single_setting(user_env_settings, positions)
        setting = get_standard_settings()
        setting.update(setting_diff)
        settings[name] = setting

    return settings, len(user_env_settings)


def get_single_setting(setting_grid: tuple, positions: list):
    """ Used by doe to get a single environment setting dict. """
    assert len(positions) == len(setting_grid)
    setting = {setting_grid[i][0]: setting_grid[i][1][p][1] for i, p in enumerate(positions)}
    name = [setting_grid[i][1][p][0] for i, p in enumerate(positions)]
    name = '_'.join(name)
    return setting, name


def store_settings_dict_to_json(settings: dict, path: str):
    if '.json' not in path:
        path += '.json'
    with open(path, 'w') as fp:
        json.dump(settings, fp, indent=4)


def load_settings_from_json(filename: str='settings/standard_settings.json'):
    if '.json' not in filename:
        filename += '.json'
    try:
        with open(filename) as fp:
            settings = json.load(fp)
    except FileNotFoundError:
        with open('settings/' + filename) as fp:
            settings = json.load(fp)
    return settings


if __name__ == '__main__':
    user_env_settings_doe()
