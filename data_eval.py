# data_eval.py
""" Evaluate data csv files.

TODO:
- plot actions
- plot storage levels
"""

import argparse
import csv
import datetime
import json
from pprint import pprint
import os
import random

import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cm as cm

import numpy as np
import pandas as pd


# TODO: Legacy code
def evaluate_tests(baseline_file: str, test_filenames: list=None,
    substring=False, plot=False, save=False, idx_from=0, idx_to=-1,
    plot_what='attacker_profit', directory: str='data', plot_actions=True):
    """ Compare profit of multiple training tests to the baseline. """
    base_df = get_dataframes_from_fullstring(fullstrings=[baseline_file], directory=directory)[0]
    # TODO: Load dfs one after the after, but not use to much storage
    if substring:
        # TODO: Currently works only for single substring
        assert len(test_filenames) == 1
        for string in test_filenames:
            test_filenames = get_filenames_from_substring(string, directory)

    test_dfs = get_dataframes_from_fullstring(fullstrings=test_filenames, directory=directory)


    # TODO: Consider same experiment with different seeds -> average them!
    # Do this with substrings where each substring indicates all files with the same settings, but only different seeds


    # All tests must have same structure and same test steps run!
    base_df = base_df.set_index('step')
    attack_metrics = {}
    for df, filename in zip(test_dfs, test_filenames):
        steps = np.array(df['step'])[idx_from:idx_to]

        # Compare reward/profit
        base_reward = np.array(base_df.loc[steps, 'reward'])
        profit = np.array(df[plot_what])[idx_from:idx_to]
        total_relative_attack_profit = (sum(profit) - sum(base_reward)) / sum(base_reward)
        attack_metrics[filename] = {
            'Total relative profit: ': total_relative_attack_profit}
        print(filename, ':')
        # print('Average relative additional profit to baseline: ',avrg_relative_attack_profit*100, '%')
        print(f'Total relative additional profit to baseline in {len(profit)} steps: {round(total_relative_attack_profit*100, 2)}%\n')

        # Compare market share
        base_total_profit = np.array(base_df.loc[steps, 'total_q_profit'])
        base_market_share = sum(base_reward) / (sum(base_total_profit))
        print(f'Non-attack market share: {round(base_market_share*100, 2)}%')
        print(sum(base_total_profit))

        attack_total_profit = np.array(df['total_q_profit'])
        attack_market_share = sum(profit) / (sum(attack_total_profit))
        print(f'Attack market share: {round(attack_market_share*100, 2)}%')
        print(sum(attack_total_profit))

        # TODO: Store dict to the results folder

    # Plot against baseline
    if plot is True or save:
        legend = []
        plt.plot(base_reward, '--')
        legend.append('base reward')
        if plot_actions:
            plt.plot(np.array((base_df.loc[steps].filter(regex='^act')).sum(axis=1))[idx_from:idx_to], '-.')
            legend.append('base actions')

        n_datapoints = max([len(df['step']) for df in test_dfs])
        if idx_to < 0:
            idx_to = idx_to+n_datapoints-1
        if idx_from < 0:
            idx_from = idx_from+n_datapoints-1
        x = range(idx_from, idx_to)
        [plt.plot(np.array(df[plot_what][idx_from:idx_to]))
              for df in test_dfs]
        legend += [f'{plot_what}_{fn}' for fn in test_filenames]
        if plot_actions:
            for df in test_dfs:
                df['summed_actions'] = (df.filter(regex='^act')).sum(axis=1)
            [plt.plot(np.array(df['summed_actions'])[idx_from:idx_to], ':')
                for df in test_dfs]
            legend += [f'sum_act_{fn}' for fn in test_filenames]

        # ax = plt.gca()  # Attempt to use the actual step indices...
        # ax.set_xticklabels(steps)
        # Add indicator to display episode endings
        full_steps = np.array(test_dfs[0]['step'])
        episode_cuts = (full_steps[0:-1]+1 != full_steps[1:]).nonzero()[0]
        episode_cuts = episode_cuts[episode_cuts>idx_from]
        episode_cuts = episode_cuts[episode_cuts<idx_to]
        plt.plot(episode_cuts, np.zeros(len(episode_cuts)), 'kx')
        legend.append('episode_ending')

        plt.legend(legend)
        plt.xlabel('Steps')
        plt.ylabel(plot_what)
        if save:
            save_file = test_filenames[0]
            save_file = save_file[:-4] + '.png'
            plt.savefig(save_file)
        else:
            plt.show()
        plt.close('all')

    return attack_metrics


def evaluate_metrics(baseline_file: str, test_name: str, directory: str, average_metrics=True, save=False, plot=False):
    directory_list = get_filenames_from_substring(test_name, directory)
    metrics_list = []
    for test_directory in directory_list:
        file_directory = directory+'/'+test_directory
        filenames = get_filenames_from_substring(
            'evaluation.csv', file_directory)
        assert len(filenames) == 1  # There should be only one eval file
        test_df = get_dataframes_from_fullstring(fullstrings=filenames, directory=file_directory)[0]
        metrics_list.append(calc_metrics(test_df))
        print('\nAttacker metrics: ', file_directory)
        pprint(metrics_list[-1])
        if save and plot is False:
            store_metrics(metrics_list[-1], file_directory+'/results.json')

    if average_metrics:
        print(f'\nDistribution of attacker metrics from {len(metrics_list)} experiments:')
        pprint(calc_metrics_distribution(metrics_list))

    if baseline_file:
        base_df = get_dataframes_from_fullstring(fullstrings=[baseline_file],directory=directory)[0]
        base_df = crop_baseline(base_df, test_df['step'])
        print('\nBaseline metrics:')
        base_metrics = calc_metrics(base_df)
        pprint(base_metrics)

    if plot:
        metrics_plot(metrics_list, base_metrics, save=save)


def store_metrics(metrics: dict, path: str):
    with open(path, 'w') as fp:
        json.dump(metrics, fp, indent=2)


def calc_metrics(df):
    metrics = {}
    metrics['n_steps'] = len(df.index)
    metrics['agent_profit'] = df['attacker_profit'].sum()
    metrics['market_share'] = (metrics['agent_profit'] /
        df['total_q_profit'].sum())
    voltage_df = df.filter(regex='^voltage_')
    metrics['n_overvoltage'] = int((voltage_df > 1.05).sum().sum())
    metrics['n_undervoltage'] = int((voltage_df < 0.95).sum().sum())
    metrics['n_line_overload'] = int((df.filter(regex='^line_load_') > 60).sum().sum())
    metrics['n_trafo_overload'] = int((df.filter(regex='^trafo_load_') > 60).sum().sum())
    return metrics


def calc_metrics_distribution(metrics_list: list):
    """ Calculate average, min/max, and standard deviation for the metrics. """
    metrics_distr = {}
    for key in metrics_list[0].keys():
        metrics_distr[key] = {}
        metrics_distr[key]['min'] = min(
            [metrics[key] for metrics in metrics_list])
        metrics_distr[key]['max'] = max(
            [metrics[key] for metrics in metrics_list])
        metrics_distr[key]['avrg'] = float(sum(
            [metrics[key] for metrics in metrics_list])) / len(metrics_list)
        metrics_distr[key]['std_dev'] = np.std(
            np.array([metrics[key] for metrics in metrics_list]))

    return metrics_distr


def crop_baseline(base_df, steps):
    """ Reduce baseline dataframe to only the test steps. """
    base_df = base_df.set_index('step')
    return base_df.loc[steps, :]


def metrics_plot(metrics_list: list, base_metrics: dict, save=''):
    # metrics_list = metrics_list[:10]
    metrics_list = sorted(metrics_list, key=lambda d: d['agent_profit'])
    keys = ('agent_profit', 'market_share', 'n_line_overload', 'n_undervoltage')
    names = ('Agent profit [k€]', 'Market share [%]', 'n overload [tsd.]', 'n undervoltage [tsd.]')

    set_font_sizes()
    fig, axs = plt.subplots(1, len(keys))
    plt.subplots_adjust(wspace=5)
    fig.set_size_inches(6, 3)
    # small: (6, 3)
    # big: (10, 5)

    for ax in axs:
        ax.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off

    colors = cm.rainbow(np.linspace(0, 1, len(metrics_list)))
    markers = [".", "o", "v", "^", "<", ">", "1", "2", "3", "4", "8",
               "s", "p", "P", "*", "h", "H", "+", "x", "X","d"]
    labels = [None] * (len(metrics_list)-3) + ['Agent 1', 'Agent 2', 'Agent x']
    random.shuffle(markers)

    for idx, key in enumerate(keys):
        metric = [m[key] for m in metrics_list]
        if key == 'market_share':
            metric = [m*100 for m in metric]
            base_metrics[key] = base_metrics[key] * 100
        else:
            metric = [m/1000 for m in metric]
            base_metrics[key] = base_metrics[key] / 1000
        axs[idx].set_xlim([-0.00001, 0.00001])
        [axs[idx].scatter([0], [met], color=colors[i], marker=markers[i], label=labels[i]) for i, met in enumerate(metric)]
        # axs[idx].axhline(y=base_metrics[key], xmin=0.1, xmax=0.9, color='black', label='Baseline')

        axs[idx].set(xlabel=names[idx])

    plt.tight_layout()
    handles, labels = axs[-1].get_legend_handles_labels()
    axs[-2].legend(handles, labels,
        loc='upper center', ncol=4, bbox_to_anchor=(-0.28, 1.13))
    # small: bbox_to_anchor=(-0.26, 1.13)

    if save:
        save =  f'{save}.pdf'
        plt.savefig(save, bbox_inches='tight', pad_inches=0)
    else:
        plt.show()
    plt.close('all')


def plot_learning(test_name: str, directory: str, save: str=''):
    directory_list = get_filenames_from_substring(test_name, directory)
    returns_list = []
    for test_directory in directory_list:
        file_directory = directory+'/'+test_directory
        filenames = get_filenames_from_substring(
            'train.csv', file_directory)
        assert len(filenames) == 1  # There should be only one such file
        test_df = get_dataframes_from_fullstring(fullstrings=filenames, directory=file_directory, add_metrics=False)[0]
        returns_list.append(np.array(test_df['return']))

    df = pd.DataFrame({idx: l for idx, l in enumerate(returns_list)})
    average_returns = np.array(df.mean(axis=1))
    std_dev_returns = np.array(df.std(axis=1))

    # Create running average of learning curve
    average_over = 10
    cumsum = np.cumsum(np.insert(average_returns, 0, 0))
    running_avrg = (cumsum[average_over:] - cumsum[:-average_over]) / float(average_over)
    cumsum = np.cumsum(np.insert(std_dev_returns, 0, 0))
    running_std_dev = (cumsum[average_over:] - cumsum[:-average_over]) / float(average_over)

    # bp = df.boxplot(column=list(df.columns))

    set_font_sizes()
    x = list(range(len(running_std_dev)))
    plt.fill_between(x, y1=running_avrg+running_std_dev, y2=running_avrg-running_std_dev, color='lightskyblue', alpha=0.4, label='Std. dev.')
    plt.plot(x, running_avrg, 'k', linewidth=2.0, label='Avrg. return')
    fig = plt.gcf()

    # [plt.plot(r, '--') for r in returns_list]

    fig.set_size_inches(6.5, 3)
    plt.ylabel('Return', labelpad=-10)
    plt.xlabel('Training episode')
    plt.legend(loc='lower right')
    # filename =  f'{os.path.dirname(directory+"/"+test_file)}/{save}_steps{idx_from}-{idx_to}.pdf'
    # plt.savefig(filename, bbox_inches='tight', pad_inches=0)

    if save:
        filename =  f'{directory}/{save}.pdf'
        plt.savefig(filename, bbox_inches='tight', pad_inches=0)
    else:
        plt.show()
    plt.close('all')


def paper_plot(baseline_file: str, test_file: str, idx_from=0, idx_to=-1,
        directory='data', include_voltages=True, include_line_load=True,
        save=False):
    """ Creates result subplots for the paper of a single (!) test file. """
    set_font_sizes()

    # Load dataframes
    base_df = get_dataframes_from_fullstring(fullstrings=[baseline_file], directory=directory)[0]
    test_df = get_dataframes_from_fullstring(fullstrings=[test_file], directory=directory)[0]
    pprint(calc_metrics(crop_baseline(base_df, np.array(test_df['step']))))
    pprint(calc_metrics(test_df))   # print just for convenience
    steps = np.array(test_df['step'])[idx_from:idx_to]
    base_df = crop_baseline(base_df, steps)

    n_plots = 2
    if include_voltages:
        n_plots += 1
    if include_line_load:
        n_plots += 1
    fig, axs = plt.subplots(n_plots)
    plt.subplots_adjust(hspace=0.42)

    # Time on x-axis
    fig.autofmt_xdate()
    start_step = test_df['step'][idx_from]
    start = str(datetime.time(int((start_step % 96)/4), int((start_step % 96)%4*15)))
    x = pd.date_range(start=start,
                      periods=len(test_df['step'][idx_from:idx_to]),
                      freq='15min')

    # plot episode cuts (just as warning that there is such a cut)
    full_steps = np.array(test_df['step'])
    episode_cuts = (full_steps[0:-1]+1 != full_steps[1:]).nonzero()[0]
    episode_cuts = episode_cuts[episode_cuts>idx_from]
    episode_cuts = episode_cuts[episode_cuts<idx_to]
    print('Episode cuts at steps: ', episode_cuts )

    # Plot profit compared to baseline
    base_profit = np.array(base_df.loc[:, 'attacker_profit'])
    test_profit = np.array(test_df['attacker_profit'])[idx_from:idx_to]

    axs[3].plot(x, test_profit, label='Attack')
    axs[3].plot(x, base_profit, '--', label='Baseline')

    axs[3].set(ylabel='Profit [€/h]')
    axs[3].set(yticks=(0, 20, 40))
    axs[3].set_title('d) Agent market profit', loc='left')

    # Plot actions compared to baseline
    n_act = sum(base_df.columns.str.count("'load', 'p_factor'"))
    base_actions = np.array(base_df.loc[steps, 'summed_actions'])/n_act
    test_actions = np.array(test_df['summed_actions'])[idx_from:idx_to]/n_act

    axs[0].plot(x, test_actions*100, label='Attack')
    axs[0].plot(x, base_actions*100, '--', label='Baseline')

    axs[0].set(ylabel='Load Sum [%]')
    axs[0].set(yticks=(20, 60, 100))
    axs[0].set_title('a) Summed relative agent load', loc='left')

    counter = 0
    if include_voltages:
        counter += 1

        base_voltage_min = np.array(base_df.loc[steps, 'min_attacker_voltage'])
        test_voltage_min = np.array(test_df['min_attacker_voltage'])[idx_from:idx_to]

        axs[counter].plot(x, test_voltage_min)
        axs[counter].plot(x, base_voltage_min, '--')
        axs[counter].hlines(0.95, x[0], x[-1], 'k',
            label='Constraints', linestyles='dotted')

        axs[counter].set(ylabel='Voltage [pu]')
        axs[counter].set(yticks=(0.95, 0.97, 0.99))
        axs[counter].set_title('b) Minimum voltage of attacker buses', loc='left')

    if include_line_load:
        counter += 1

        base_load_max = np.array(base_df.loc[:, 'max_attacker_line_load'])
        test_load_max = np.array(test_df['max_attacker_line_load'])[idx_from:idx_to]

        axs[counter].plot(x, test_load_max)
        axs[counter].plot(x, base_load_max, '--')
        axs[counter].hlines(60, x[0], x[-1], 'k', linestyles='dotted',
            label='Constraints')

        axs[counter].set(ylabel='Line Load [%]')
        axs[counter].set(yticks=(20, 40, 60))
        axs[counter].set_title('c) Maximum line load in attacker feeder', loc='left')

    locator = mdates.AutoDateLocator(minticks=12, maxticks=16)
    formatter = mdates.DateFormatter('%H:%M')
    for ax in axs:
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
    axs[-1].set(xlabel='Daytime')

    # Set a single legend for all sub-plots
    handles0, labels0 = axs[0].get_legend_handles_labels()
    handles1, labels1 = axs[2].get_legend_handles_labels()
    axs[0].legend(handles0+handles1, labels0+labels1,
        loc='upper right', ncol=3, bbox_to_anchor=(1, 1.4))

    [ax.grid(True, which='both', axis='x', alpha=0.2) for ax in axs]
    fig.set_size_inches(7.6, 5.5)
    if save:
        filename = f'{os.path.dirname(directory+"/"+test_file)}/{save}_steps{idx_from}-{idx_to}.pdf'
        plt.savefig(filename)
    else:
        plt.show()
    plt.close('all')


def simbench_plot(save=''):
    import pandapower as pp
    import pandapower.plotting as plot
    import env_settings
    import environment
    from pandapower.plotting.plotting_toolbox import get_collection_sizes
    fig, ax = plt.subplots(1)
    env = environment.QMarketEnv(**env_settings.get_standard_settings())

    net = env.net
    pp.runpp(net)

    line_width=1.3
    bus_size=0.6
    sizes = get_collection_sizes(net, bus_size)
    bus_size = sizes["bus"]
    switch_size = sizes["switch"]
    switch_distance = sizes["switch_distance"]

    attacker_buses = np.array([61, 62, 63, 64, 65, 66, 67, 68])
    abc = plot.create_bus_collection(net, attacker_buses,
        size=bus_size, color='red', zorder=10, patch_type='poly4')
    nbc = plot.create_bus_collection(net,
        set(net.bus.index)-set(attacker_buses),
        size=bus_size, color='blue', zorder=2)

    nogolines = set(net.switch.element[(net.switch.et == "l") & (net.switch.closed == 0)])
    lines = set(env.net.line.index)
    lc = plot.create_line_collection(net, lines-nogolines, zorder=1, color='black', linewidths=line_width)
    olc = plot.create_line_collection(net, nogolines, zorder=1, color='grey', linewidths=line_width/2, linestyle='dotted')

    plot.draw_collections([lc, olc, abc, nbc], figsize=(5,2.3), ax=ax, plot_colorbars=False, draw=False)

    if save:
        print(save)
        if 'pdf' not in save and 'png' not in save:
            save =  f'{save}.png'
        if 'png' in save:
            plt.savefig(save, bbox_inches='tight', pad_inches=0, dpi=1000)
        else:
            plt.savefig(save, bbox_inches='tight', pad_inches=0)
    else:
        plt.show()
    plt.close('all')


def set_font_sizes():
    SMALL_SIZE = 8
    MEDIUM_SIZE = 9
    BIGGER_SIZE = 10

    plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


def plot_baseline(file_ending: str, frame: tuple, plot_rewards=True, plot_actions=True, plot_voltages=True):
    dfs, filenames = get_dataframes_from_substring(file_ending)

    if plot_rewards:
        for df in dfs:
            plt.plot(df['reward'][frame[0]:frame[1]])

    plt.gca().set_prop_cycle(None)
    if plot_actions:
        for df in dfs:
            plt.plot(df.summed_actions[frame[0]:frame[1]], '--')

    if plot_voltages:
        for df in dfs:
            plt.plot(df['max_voltage'][frame[0]:frame[1]])
        for df in dfs:
            plt.plot(df['min_voltage'][frame[0]:frame[1]])

    leg = []
    if plot_rewards:
        leg += [f'reward_{idx}' for idx in range(len(dfs))]
    if plot_actions:
        leg += [f'actions_{idx}' for idx in range(len(dfs))]
    plt.legend(leg)

    plt.show()
    plt.close('all')


def get_filenames_from_substring(substring: str, directory: str='data'):
    if directory[-1] != '/':
        directory += '/'
    return [fn for fn in os.listdir(directory) if substring in fn]


def get_dataframes_from_fullstring(fullstrings: list, directory: str='data', add_metrics=True):
    # TODO: Change to single df or change to generator function!
    dfs = []
    for filename in fullstrings:
        if directory not in filename:
            filename = directory + '/' + filename
        df = pd.read_csv(filename)
        dfs.append(df)

    if not add_metrics:
        return dfs

    for df in dfs:
        add_metrics_to_df(df)

    return dfs


def add_metrics_to_df(df):
    """ Compute some metrics that were not stored in simulation. """
    # Uses the true actions that were actually performed (not what the agent wanted to do)
    df['summed_actions'] = (df.filter(regex='^act')).sum(axis=1)

    buses = (61, 62, 63, 64, 65, 66, 67, 68)
    voltage_df = df[[f'voltage_{b}' for b in buses]]
    df['avrg_attacker_voltage'] = voltage_df.mean(axis=1)
    df['min_attacker_voltage'] = voltage_df.min(axis=1)
    df['max_attacker_voltage'] = voltage_df.max(axis=1)

    lines = range(44, 65)  # Whole feeder 6
    # only_attacker_lines = range(58, 65)  # Sub-feeder; only connect attacker buses (just as a note, if required later)
    line_load_df = df[[f'line_load_{l}' for l in lines]]
    df['avrg_attacker_line_load'] = line_load_df.mean(axis=1)
    df['min_attacker_line_load'] = line_load_df.min(axis=1)
    df['max_attacker_line_load'] = line_load_df.max(axis=1)

    return df


def voltage_boxplots(filename: str, directory='data/'):
    df = pd.read_csv(directory + filename)
    df = df.filter(regex='^voltage', axis=1)
    bp = df.boxplot(column=list(df.columns))
    plt.show()


def main():
    choices = {'reward': 'reward', 'profit': 'attacker_profit'}
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--baseline-file',
        default='data/2021-02-10T09:43:47.640569_5643144_baseline_standard_settings/baseline_data.csv',
        help="Name of csv file where the results of the baseline were stored")
    argparser.add_argument(
        '--test-files',
        default='TD3_Results/very_first_run_standard_setting/model.pth.150.csv',
        help="Name of files where test results are stored."
    )
    argparser.add_argument(
        '--plot',
        help="Plot the rewards of test compared to baseline live",
        action='store_true'
    )
    argparser.add_argument(
        '--save',
        help="Filename to save results to? Sets plotting to True automatically",
        default=''
    )
    argparser.add_argument(
        '--from-idx',
        help="Starting point of evaluation as index of the test file",
        default=0,
        type=int
    )
    argparser.add_argument(
        '--to-idx',
        help="End point of evaluation as index of the test file",
        default=-1,
        type=int
    )
    argparser.add_argument(
        '--plot-only',
        help="Plot only: 'reward', 'profit' (default)",
        default='profit',
        choices=choices.keys()
    )
    argparser.add_argument(
        '--directory',
        help="Directory where the files are (Default: 'data')",
        default='data',
    )
    argparser.add_argument(
        '--paper-plot',
        help="Create plot for actual use in paper",
        action='store_true'
    )
    argparser.add_argument(
        '--metrics',
        help="Print attack metrics",
        action='store_true'
    )
    argparser.add_argument(
        '--learning-plot',
        help="Plot the learning process?",
        action='store_true'
    )
    argparser.add_argument(
        '--simbench-plot',
        help="Plot the simbench net?",
        action='store_true'
    )
    args = argparser.parse_args()
    test_files = args.test_files.split(' ')

    if args.simbench_plot:
        simbench_plot(save=args.save)
    elif args.metrics:
        assert len(test_files) == 1
        evaluate_metrics(
            baseline_file=args.baseline_file,
            test_name=test_files[0],
            directory=args.directory,
            save=args.save,
            plot=args.plot)
    elif args.paper_plot:
        assert len(test_files) == 1
        paper_plot(
            args.baseline_file,
            test_files[0],
            idx_from=args.from_idx,
            idx_to=args.to_idx,
            directory=args.directory,
            save=args.save)
    elif args.learning_plot:
        plot_learning(test_files[0], args.directory, args.save)


if __name__ == '__main__':
    main()
