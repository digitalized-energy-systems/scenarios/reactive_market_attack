## General Information
This is the code that was used for the publication https://doi.org/10.1186/s42162-021-00181-5.

If you re-use part of the code for research, please cite the publication.

The experiment results and trained models from the paper can be found here: https://gitlab.com/digitalized-energy-systems/data/data_reactive_market_attack

For questions, suggestions, etc. contact me per email (thomas.wolgast@uni-oldenburg.de) or create an issue here.

## Installation
We recommend to use Docker to run the code.
* Make sure docker is installed
* Run `docker build -t <IMAGE_NAME> .`

## Installation without Docker
* Create some kind of virtual environment with python 3.8 in anaconda, pipenv etc.
* Within the virtual environment, install pytorch (in anaconda for example with `conda install pytorch cpuonly -c pytorch`. For details see here: https://pytorch.org/)
* Run `pip install -r requirements.txt`

## Run an experiment with Docker
* Run `docker run -u $(id -u) -v "$PWD"/data:/workspace/data <IMAGE_NAME> python3 /workspace/main.py --agent-type td3`
* Use the `--help` flag to display information how to store results, use user-defined settings, or multiprocessing.

## Run an experiment without Docker
* Run `python3 main.py --agent-type td3`
* Use the `--help` flag to display information how to store results, use user-defined settings, or multiprocessing.

## How to recreate plots from paper

For most plots the experiment results are required. Pull the data from the URL above into a `data/` folder in this directory. If you name the folder differently, change the paths in the following commands.

The learning curve: `python data_eval.py --dir data/data_reactive_market_attack/td3 --base 2021-02-11T16:32:30.806682_13554927_baseline_standard_settings/evaluation.csv --test td3_standard --learn`

The metrics plot: `python data_eval.py --dir data/data_reactive_market_attack/td3 --base 2021-02-11T16:32:30.806682_13554927_baseline_standard_settings/evaluation.csv --test td3_standard --metrics --plot` (remove `plot` to just print the metrics)

The window plot: `python data_eval.py --dir data/data_reactive_market_attack/td3 --base 2021-02-11T16:32:30.806682_13554927_baseline_standard_settings/evaluation.csv --test 2021-02-15T14:01:12.221586_9114961_td3_standard_settings/td3_model.pth_evaluation.csv --paper --from 3080 --to 3330`

The simbench network: `python data_eval.py --simbench`

Add `--save <filename>` to automatically save the plot.
