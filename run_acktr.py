from pathlib import Path
from rainy.lib import kfac
from rainy import Config, net
from rainy.envs import MultiProcEnv
from rainy.agents import ACKTRAgent
from rainy.experiment import Experiment
from rainy.net.policy import SeparateStdGaussianDist

import test_agent


def run_acktr(env, settings: dict, path: str=None, get_model_for_test=False,
    autotest=True):
    if not path:
        path = 'data/ACKTR_Results/Temp'
    tau = 12 * 20
    update_freq = 10
    c = Config()

    c.max_steps = int(1e5)
    c.nworkers = 12
    c.parallel_seeds = [settings['seed']+n*1000 for n in range(c.nworkers)]
    c.nsteps = 20
    c.set_net_fn(
        "actor-critic",
        net.actor_critic.fc_shared(policy=SeparateStdGaussianDist)
    )
    c.set_parallel_env(MultiProcEnv)
    c.set_optimizer(kfac.default_sgd(eta_max=0.02))  # 0.1 original
    c.set_preconditioner(
        lambda net: kfac.KfacPreConditioner(
            net,
            tau=tau,
            update_freq=update_freq,
            norm_scaler=kfac.SquaredFisherScaler(eta_max=0.1, delta=0.001),
        )
    )
    c.grad_clip = 0.5  # 5 was original, but 0.5 is recommended
    c.gae_lambda = 0.95
    c.use_gae = True
    c.eval_deterministic = True
    c.value_loss_weight = 0.5
    c.entropy_weight = 0.0
    c.eval_freq = None
    c.save_freq = 10000
    c.logger.logdir = Path(path)
    c.logger.LOG_CAPACITY = 30 # TODO

    env.autoscale_obs = True  # Autoscale observations to range [0...1]
    env.use_reward_monitor = False
    c.set_env(lambda: env)
    c.action_dim = env.action_space.shape
    c.state_dim = env.observation_space.shape

    agent = ACKTRAgent(c)
    save_file_name = "acktr_model.pth" if path else None

    experiment = Experiment(agent, save_file_name)
    if get_model_for_test:
        return experiment

    experiment.train()

    if autotest:
        test_agent.run_test(nn_path=path+save_file_name, rainy_type='acktr')
