# run_baseline.py
""" Run random agent as baseline for some environment. """

import numpy as np

import environment
from to_csv import Store


def exact_baseline(env, settings: dict, path: str, only_test_data=False, last_step: int=None, actions='baseline'):
    """ Run a single episode of some timeframe. """


    # Overwrite env, because baseline = no actual attackers
    settings['load_actuators_only'] = True  # Do not utilize sgen attackers
    settings['n_past_obs'] = 1  # Make sure the first step is simulated
    del settings['test_episodes']  # Set to "no testing"
    env = environment.QMarketEnv(**settings)

    if path:
        data_file = path + 'evaluation.csv'
        store = Store(filename=data_file)
    else:
        data_file = None

    observation = env.reset('full')
    if last_step is not None:
        # Running the whole year would take to long?
        env.last_step = last_step

    done = False
    while not done:
        observation, reward, done, _ = env.step(actions=actions)

        if data_file:
            data = env.get_health_metrics(verbose=True)
            data['reward'] = reward
            data['step'] = env.current_step
            data.update({f'obs{i}: ' + str(env.observation_space_keys[i]): obs
                         for i, obs in enumerate(env.current_obs)})
            data.update({f'act{i}: ' + str(env.action_keys[i]): act
                         for i, act in enumerate(env.true_actions)})
            store.to_csv(data)