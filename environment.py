# environment.py
"""
The environment for the e-energy paper 2021. Consisting of a power grid, a reactive power market, and some attacker agents. Similar to gym API.

TODO: Explain load/storage model here!

"""

import copy
from datetime import datetime
import logging
import math
from pprint import pprint
import random
from rainy.envs import EnvSpec, EnvTransition

import gym
import numpy as np
import pandapower.networks as pn
import pandapower as pp

from constraint_handler import ConstraintHandler
from qmarket import define_qmarket, update_unit_constraints, get_reactive_profits
from pp_util import relax_constraints

import pdb

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ResetFailure(Exception):
    pass

class QMarketEnv():
    def __init__(self,
            grid_id,  # *change requires to compute new baseline
            attacking_loads,  # *
            attacking_sgens,  # *
            load_actuators_only=True,
            prevent_overflow=False,
            prevent_underflow=False,
            storage_init='random',
            load_costs_calc='overflow',  # *
            steps_per_storage=4,
            train_epis_length=1*24*4,
            controllable_share=1.0,  # *
            p_price_euro_per_mwh=30,  # *
            q_price_euro_per_mvarh2=250,  # *
            profit_factor_q=2,  # *
            scaling_sgens=1,  # *
            scaling_loads=1,  # *
            n_past_obs=2,
            fail_strategy='next_step',
            action_penalty_factor=0,  # Penalize illegal actions?
            autoscale_obs=False,  # Scale to [0...1] automatically?
            test_episodes=[],
            train_on_test=False,
            seed=None,
            log_level=None):
        self.seed(seed)
        if log_level:
            logger.setLevel(log_level)
        # To what extent are the loads controllable/shiftable?
        self.controllable_share = controllable_share
        self.steps_per_storage = steps_per_storage
        self.train_epis_length = train_epis_length
        self.p_price_euro_per_mwh = p_price_euro_per_mwh
        # Attacker can only control loads? Or generators as well?
        self.load_actuators_only = load_actuators_only
        self.attacking_sgens = attacking_sgens
        self.attacking_loads = attacking_loads

        self.profit_factor_q = profit_factor_q
        self.q_price_euro_per_mvarh2 = q_price_euro_per_mvarh2
        self.scaling_loads = scaling_loads
        self.scaling_sgens = scaling_sgens
        self._init_qmarket_net(grid_id)

        assert n_past_obs > 0
        self.n_past_obs = n_past_obs

        assert set(attacking_sgens).issubset(set(self.net.sgen.index))
        assert set(attacking_loads).issubset(set(self.net.load.index))
        self.action_space = None
        self.observation_space = None
        self._set_action_space()
        self._autoscale_obs = autoscale_obs
        self._set_observation_space()
        self._spec = EnvSpec(state_dim=self.observation_space.shape,
                             action_space=self.action_space)

        if storage_init == 'random':
            # TODO: build setter!
            self.set_storage_levels = self._set_storage_levels_random
        elif storage_init == 'full':
            self.set_storage_levels = self._set_storage_levels_to_1
        self.load_costs_calc = load_costs_calc
        self.prevent_overflow = prevent_overflow
        self.prevent_underflow = prevent_underflow
        self.action_penalty_factor = action_penalty_factor
        self.fail_strategy = fail_strategy

        self._set_test_data(test_episodes, train_on_test)

    def _init_qmarket_net(self, grid_id):
        self.net, self.profiles = define_qmarket(
            grid_id, self.p_price_euro_per_mwh, self.scaling_loads,
            self.scaling_sgens,
            q_price_euro_per_mvarh2=self.q_price_euro_per_mvarh2,
            profit_factor_q=self.profit_factor_q)
        self.constraint_handler = ConstraintHandler(self.net,
            delta_u=0.005, delta_line_loading=5, delta_trafo_loading=5)
        self.n_constraint_relaxes = 5  # TODO: Move into the ConstrHandler
        self.opf_failure_counter = 0

        assert len(self.net.gen.index) == 0, 'Gen support not implemented'
        self.n_steps = len(self.profiles[('sgen', 'p_mw')])
        self._add_attacker_loads(self.net)
        self._add_attacker_sgens(self.net)

    def _add_attacker_loads(self, net):
        # Max controllable load is used to fill the "storage" of the load
        net.load['storage_power_p_mw'] = (self.controllable_share
            * self.profiles[('load', 'p_mw')].max(axis=0))
        net.load['storage_power_q_mvar'] = (self.controllable_share
            * self.profiles[('load', 'q_mvar')].max(axis=0))
        net.load['storage_capacity'] = (net.load['storage_power_p_mw']
            * self.steps_per_storage)

        # The load loses some storage level every time step
        net.load['storage_loss_pu'] = 0.0
        net.load['p_factor'] = 0.0

    def _add_attacker_sgens(self, net):
        """ Adjust pandapower network so that active power generation of sgens can be used as actuator. """
        # The agent can set a factor [0...1] of the max possible power to set
        net.sgen['p_factor'] = 1.0

    def _set_action_space(self):
        self.action_keys = [('load', 'p_factor', idx)
                            for idx in self.attacking_loads]
        if not self.load_actuators_only:
            self.action_keys += [('sgen', 'p_factor', idx)
                                 for idx in self.attacking_sgens]

        self.action_space = gym.spaces.Box(
            low=0.0, high=1.0,
            shape=(len(self.action_keys),),
            dtype=np.float32)

        if self._seed:
            self.action_space.seed(self._seed)

    @property
    def autoscale_obs(self):
        return self._autoscale_obs

    @autoscale_obs.setter
    def autoscale_obs(self, setpoint: bool):
        self._autoscale_obs = setpoint
        self._set_observation_space()

    def _set_observation_space(self):
        """ The observation space of the attackers: local voltage values,
        local active and reactive power values of sgens and loads, storage
        level of loads.

        Also store the key tuples to easily access all observations later in
        simulation."""

        self.visible_buses = tuple(set([self.net[key[0]]['bus'][key[2]]
                                        for key in self.action_keys]))

        voltages = self._observed_voltages()
        load_storage_levels = self._observed_storage_levels()
        load_active_power = self._observed_unit_power('load', 'p_mw')
        load_reactive_power = self._observed_unit_power('load', 'q_mvar')
        sgen_active_power = self._observed_unit_power('sgen', 'p_mw')
        sgen_reactive_power = self._observed_unit_power('sgen', 'q_mvar')
        future_load_power = self._observed_next_unit_power('load')
        future_sgen_power = self._observed_next_unit_power('sgen')
        daytime = self._observed_daytime()

        # Add future values? (of own units only!)
        # Maybe sensitivity values.
        # Maybe weather values (or rather max sgen active power!)
        # add average voltage?

        # TODO: Maybe simply add up power values that are on the same bus to reduce space?!

        observation_space = (voltages[0] + load_storage_levels[0]
            + load_active_power[0] + load_reactive_power[0]
            + sgen_active_power[0] + sgen_reactive_power[0]
            + daytime[0] + future_load_power[0] + future_sgen_power[0])

        # TODO: don't use flattened arrays? to distinguish between steps!
        # observation_space *= self.n_past_obs

        self.low = np.array([value[0] for value in observation_space])
        self.high = np.array([value[1] for value in observation_space])
        if self._autoscale_obs:
            self.observation_space = gym.spaces.Box(
                low=np.zeros(len(self.low)*self.n_past_obs),
                high=np.ones(len(self.high)*self.n_past_obs),
                dtype=np.float64)
        else:
            self.observation_space = gym.spaces.Box(
                low=np.tile(self.low, self.n_past_obs),
                high=np.tile(self.high, self.n_past_obs),
                dtype=np.float64)

        self.observation_space_keys = (voltages[1] + load_storage_levels[1]
            + load_active_power[1] + load_reactive_power[1]
            + sgen_active_power[1] + sgen_reactive_power[1]
            + daytime[1] + future_load_power[1] + future_sgen_power[1])

        # Make sure there are no double entries
        assert len(self.observation_space_keys) == len(set(self.observation_space_keys))
        assert (len(self.observation_space_keys) * self.n_past_obs
            == self.observation_space.shape[0])

    def _observed_voltages(self):
        # Assumption: These are the max/min voltage values
        max_deviation = self.constraint_handler.delta_u * self.n_constraint_relaxes
        voltages = [(0.95-max_deviation, 1.05+max_deviation)
                    for _ in self.visible_buses]
        voltage_keys = [('res_bus', 'vm_pu', idx)
                        for idx in self.visible_buses]
        return voltages, voltage_keys

    def _observed_storage_levels(self):
        load_storage_levels = [(0.0, 1.0) for _ in self.attacking_loads]
        load_storage_level_keys = [('load', 'storage_level_pu', idx)
                                   for idx in self.attacking_loads]
        return load_storage_levels, load_storage_level_keys

    def _observed_unit_power(self, unit_type: str, pq: str):
        attackers = self.__dict__[f'attacking_{unit_type}s']
        df = self.net[unit_type]
        active_reactive_power_range = [
            (df[f'min_{pq}_tech'][idx],
             df[f'max_{pq}_tech'][idx])
            for idx in attackers]
        active_reactive_power_keys = [
            (f'res_{unit_type}', pq, idx) for idx in attackers]
        return active_reactive_power_range, active_reactive_power_keys

    def _observed_next_unit_power(self, unit_type: str, pq='p_mw'):
        attackers = self.__dict__[f'attacking_{unit_type}s']
        df = self.net[unit_type]
        # The values from the profiles need to be unscaled!
        next_power_range = [
            (df[f'min_{pq}_tech'][idx] / df['scaling'][idx],
             df[f'max_{pq}_tech'][idx] / df['scaling'][idx])
            for idx in attackers]
        next_power_keys = [
            ((unit_type, pq), idx, 1)
            for idx in attackers]

        return next_power_range, next_power_keys

    def _observed_daytime(self):
        """ Time observation which time in the day it is in hourly resolution. """
        daytime = [(-1.0, 1.0) for _ in range(6)]
        daytime_keys = ['day_sin', 'day_cos', 'week_sin', 'week_cos',
                        'year_sin', 'year_cos']

        return daytime, daytime_keys

    def _set_test_data(self, test_episodes: list, train_on_test: bool):
        """ Save some test episodes as a blacklist that is not used for
        training, but can be iterated in testing. """
        self.test_episodes = test_episodes
        for episode in test_episodes:
            assert episode[-1] <= self.n_steps

        # Create a whitelist of possible starts for training episodes
        whitelist = set(range(-1, self.n_steps-self.train_epis_length))
        if not train_on_test:
            for episode in test_episodes:
                whitelist -= set(range(episode[0]-self.train_epis_length+1,
                                       episode[-1]-self.n_past_obs))
        # TODO: Add possibility of shorter episodes to reduce overlap and therefore increase training data -> Introduce self.min_eps_lenght!
        self.whitelist = tuple(whitelist)

    def get_health_metrics(self, verbose=False):
        """ Return some metrics about the grid state to determine if successful attacks take place. """
        net = self.net
        metrics = {}
        if verbose:
            metrics.update({f'voltage_{i}': net.res_bus.vm_pu[i]
                           for i in net.res_bus.index})
            metrics.update({f'line_load_{i}': net.res_line.loading_percent[i]
                           for i in net.res_line.index})
            metrics.update({f'trafo_load_{i}':
                           net.res_trafo.loading_percent[i]
                           for i in net.res_trafo.index})
        else:
            metrics['max_voltage'] = net.res_bus.vm_pu.max()
            metrics['max_voltage_bus'] = net.res_bus.vm_pu.idxmax()
            metrics['min_voltage'] = net.res_bus.vm_pu.min()
            metrics['min_voltage_bus'] = net.res_bus.vm_pu.idxmin()
            metrics['avrg_voltage'] = net.res_bus.vm_pu.mean()
            metrics['max_line_loading_idx'] = net.res_line.loading_percent.idxmax()
            metrics['max_line_loading'] = net.res_line.loading_percent.max()
            metrics['max_trafo_loading_idx'] = net.res_trafo.loading_percent.idxmax()
            metrics['max_trafo_loading'] = net.res_trafo.loading_percent.max()
        # TODO: Add verbose variant as well?
        metrics['load_sum'] = sum(net.res_load.p_mw)
        # Actual active power values (after possible reduction)
        # TODO: muss aktualisiert werden?!
        metrics['gen_sum_reduced'] = sum(net.res_gen.p_mw) + sum(net.res_sgen.p_mw)
        # Maximum possible active power values from timeseries data
        metrics['gen_sum_max'] = sum(self.profiles[('sgen', 'p_mw')].loc[self.current_step]) * self.scaling_sgens

        assert len(net.gen.index) == 0  # Add gens only if required!

        # Constraint satisfaction?
        metrics['OPF_relaxations'] = self.constraint_handler.counter
        metrics['n_overvolt'] = sum(net.res_bus.vm_pu >
            self.constraint_handler.u_max)
        metrics['n_undervolt'] = sum(net.res_bus.vm_pu <
            self.constraint_handler.u_min)
        metrics['n_line_overload'] = sum(net.res_line.loading_percent >
            self.constraint_handler.max_line_loading)
        metrics['n_trafo_overload'] = sum(net.res_trafo.loading_percent >
            self.constraint_handler.max_trafo_loading)

        # Market metrics
        metrics['total_q_profit'] = (get_reactive_profits(
            net, unit_keys={'sgen': list(net.sgen.index)})
            * (1 - 1/self.profit_factor_q))  # = profit for all units
        metrics['attacker_profit'] = self.profit
        metrics['attacker_costs'] = self.costs
        metrics['attacker_penalty'] = self.penalty

        return metrics

    def _set_time_step(self, net, step: int):
        """ Set the net to a desired time step from the timeseries. """
        logger.debug(f'Set time step {step} (Last step: {self.last_step})')
        for type_act in self.profiles.keys():
            if not self.profiles[type_act].shape[1]:
                continue
            unit_type, actuator = type_act
            net[unit_type].loc[:, actuator] = self.profiles[type_act].loc[step]
            if unit_type == 'load':
                self._update_loads(net, step, type_act)
        net.sgen['p_mw_original'] = net.sgen.p_mw

    def _update_loads(self, net, step: int, type_act: tuple):
        # Instead of using the active power value directly, use it
        # as loss for the internal storage of the load
        if type_act[1] == 'p_mw':
            if self.controllable_share > 0:
                net.load.loc[self.attacking_loads, 'storage_loss_pu'] = (
                    self.profiles[type_act].loc[step]
                    * self.controllable_share
                    / self.net.load['storage_capacity'])
            else:
                net.load.loc[self.attacking_loads, 'storage_loss_pu'] = 0.0

        net.load.loc[self.attacking_loads, type_act[1]] = (
            (1-self.controllable_share) * self.profiles[type_act].loc[step])

    def step(self, actions=None, ignore_actions=False):
        assert self.done is False
        self.current_step += 1
        if isinstance(actions, str) and actions == 'baseline':
            baseline = True
            actions = self._baseline_actions()
        if isinstance(actions, str) and actions == 'random':
            actions = self._random_actions()
            baseline = False
        else:
            baseline = False
        self._set_time_step(self.net, self.current_step)
        if not ignore_actions:
            self._apply_actions(self.net, actions)
        update_unit_constraints(self.net)

        try:
            self._market_clearing(self.net)
            reward = self._reward_fct(self.net)
        except (pp.optimal_powerflow.OPFNotConverged,
                pp.powerflow.LoadflowNotConverged):
            logger.error('-------------------------------------------')
            if self.fail_strategy == 'next_step':
                logger.error('OPF completely failed -> Jump to next step!')
                if not baseline:
                    # Actions were applied already -> ignore them!
                    return self.step(ignore_actions=True)
                else:
                    # Create new baseline actions for the next step
                    return self.step(actions='baseline')
            logger.error('OPF completely failed -> Gracefully end episode!')
            # Set p to controllable, if not possible, before?
            # Graceful exit not good for testing, because only one episode...
            pp.runpp(self.net)
            self.done = True
            # No market clearing -> no profit, but only costs
            reward = self._reward_fct(self.net, ignore_profit=True)

        self._create_stack_of_observations(self.net)

        if self.current_step >= self.last_step:
            logger.debug('Environment done \n')
            self.done = True

        info = None  # TODO: Similar to openai gym?

        if reward >= 5:  # TODO: Make env-independent (eg >= 5*avrg_reward)
            logger.info('---------------Sucessful attack?!---------------')

        return EnvTransition(self.obs.flatten(), reward, self.done, info)

    def step_and_reset(self, action) -> EnvTransition:
        logger.debug(f"Step and reset with action: {action}")
        transition = self.step(action)
        # transition = EnvTransition(
        #     state=res[0],
        #     reward=res[1],
        #     terminal=res[2],
        #     info=res[3],
        # )
        if transition.terminal:
            logger.info('Rainy: Reset environment')
            return EnvTransition(self.reset(), *transition[1:])
        else:
            return transition

    def _apply_actions(self, net, actions):
        """ Update network with agent actions. """
        logger.debug(f"Applying agent actions to grid: {actions}")
        assert not np.isnan(actions).any()
        self.original_actions = copy.copy(actions)
        actions = np.clip(actions, a_min=0.0, a_max=1.0)
        self.clipped_actions = copy.copy(actions)
        if self.prevent_overflow is True or self.prevent_underflow is True:
            self._enforce_load_constraints(net, actions)
        self.true_actions = actions

        for key, action_value in zip(self.action_keys, actions):
            unit_type, actuator, idx = key
            net[unit_type][actuator][idx] = action_value

        if not self.load_actuators_only:
            # TODO: Make sure it's applied correctly!
            net.sgen.p_mw *= net.sgen.p_factor

        net.load.p_mw += (net.load['storage_power_p_mw']
            * net.load['p_factor'])
        # Assumption: Reactive power coupled to active power of loads
        net.load.q_mvar += (net.load['storage_power_q_mvar']
            * net.load['p_factor'])

        net.load['storage_level_pu'] += (
            net.load['p_factor'] / self.steps_per_storage)
        net.load['storage_level_pu'] -= net.load['storage_loss_pu']

        # Storage must not overflow/underflow -> cut off
        net.load['storage_level_pu_uncut'] = net.load['storage_level_pu']
        overflow = net.load['storage_level_pu'] > 1.0
        underflow = net.load['storage_level_pu'] < 0.0
        net.load['storage_level_pu'][overflow] = 1.0
        net.load['storage_level_pu'][underflow] = 0.0

    def _enforce_load_constraints(self, net, actions: list):
        """ Ensure that storage level does not get negative and that storage
        does not overflow. """
        strg_level_after_loss = net.load['storage_level_pu'] - net.load['storage_loss_pu']
        imag_strg_level_pu = (strg_level_after_loss[self.attacking_loads]
            + actions[0:len(self.attacking_loads)] / self.steps_per_storage)

        underflow, overflow = [], []
        for j, (unit_type, actuator, i) in enumerate(self.action_keys):
            if not unit_type == 'load' or not actuator == 'p_factor':
                continue
            if self.prevent_underflow and imag_strg_level_pu[i] < 0:
                underflow.append(i)
                actions[j] = -strg_level_after_loss[i] * self.steps_per_storage
            elif self.prevent_overflow and imag_strg_level_pu[i] > 1:
                overflow.append(i)
                actions[j] = (1.0 - strg_level_after_loss[i]) * self.steps_per_storage
        if underflow:
            logger.debug(
                f"storages {underflow} would run empty! -> Increase P")
        if overflow:
            logger.debug(
                f"storages {overflow} would overflow! -> Decrease P")

    def _reward_fct(self, net, ignore_profit=False) -> float:
        """ Calculate reward from calculated pandapower network. """
        self.profit = 0
        if not ignore_profit:
            self.profit = self._reactive_profit(net)
            logger.debug(f'Sgen profit: {self.profit}')
        self.costs = self._load_costs(net)
        logger.debug(f'Load costs: {self.costs}')
        if not self.load_actuators_only:
            sgen_costs = self._sgen_costs(net)
            logger.debug(f'Sgen costs: {sgen_costs}')
            self.costs += sgen_costs
        self.penalty = self._penalty_costs()
        logger.debug(f'Penalty for illegal actions: {self.penalty}')
        reward = self.profit - self.costs - self.penalty
        assert not math.isnan(reward)
        return reward

    def _reactive_profit(self, net):
        """ Return profit from reactive power including internal costs. """
        revenue = get_reactive_profits(
            net, unit_keys={'sgen': self.attacking_sgens})
        return revenue * (1 - 1/self.profit_factor_q)

    def _load_costs(self, net) -> float:
        """ Determine additional active power costs for the controllable
        loads. Only use resulting costs from overflowing, because all other costs would emerge anyway. """
        # Assumption: Same active power price for all loads (but 10 times higher than for sgens)
        # Assumption: No costs for load reactive power
        cut_off_pu = (net.load['storage_level_pu_uncut']
            - net.load['storage_level_pu'])
        cut_off_mw = (net.load['storage_capacity'] * net.load['scaling']
            * cut_off_pu)

        if self.load_costs_calc == 'overflow':
            # How much energy was wasted?
            # TODO: Is this still required or can be deleted
            # Does not really make sense because agents only have costs, if they waste energy, but normal energy consuming is free!
            total_loss_mw = sum(
                cut_off_mw[self.attacking_loads][cut_off_pu>0.0])
        elif self.load_costs_calc == 'diff_baseline':
            # How much energy was bought compared to the baseline case?
            df = self.profiles[('load', 'p_mw')]
            baseline_loads = df.loc[self.current_step, self.attacking_loads]
            load_loss_mw =  net.load.scaling[self.attacking_loads] * (net.load.p_mw[self.attacking_loads] - baseline_loads )
            total_loss_mw = sum(load_loss_mw) * self.controllable_share
        elif self.load_costs_calc == 'none':
            return 0
        # TODO
        # elif self.load_costs_calc == 'episode_end_diff':
        # Calculate active power costs with storage level diff at end of episode (+overflow!!!)
        # elif self.load_costs_calc == 'automatic_balancing':
        # Automatically balance load power in a way that the costs are zero, ignore agent actions for that at the end of the episode

        # How much energy would have been required to fulfill task?
        total_non_fulfilment_mw = sum(cut_off_mw[self.attacking_loads][cut_off_pu<0.0])

        return (total_loss_mw * 10 - total_non_fulfilment_mw * 10)  # * self.p_price_euro_per_mwh)

    def _sgen_costs(self, net) -> float:
        """ Determine costs for non-maximum active power values of sgens. """
        p_reduction = (
            net.sgen.p_mw_original - net.sgen.p_mw)[self.attacking_sgens]
        p_reduction *= net.sgen.scaling[self.attacking_sgens]
        total_p_reduction = sum(p_reduction)
        # Assumption: same active power price for all units
        return total_p_reduction * self.p_price_euro_per_mwh

    def _penalty_costs(self):
        total_deviation = sum((self.original_actions - self.true_actions)**2)
        return total_deviation * self.action_penalty_factor

    def _market_clearing(self, net):
        """ The grid operator simply buys the cheapest amount of reactive power (and maybe active power reduction) that solves all constraint violations. -> Optimal Reactive Power Flow """
        self._opf_prehandling(net)
        self._optimal_power_flow(net)

    def _opf_prehandling(self, net):
        self.constraint_handler.reset_constraints(net)
        if self.last_q_values is None:
            self.last_q_values = np.zeros(len(net.sgen.q_mvar))
        else:
            self.last_q_values = net.res_sgen.q_mvar

    def _optimal_power_flow(self, net):
        try:
            pp.runopp(net, init='flat')
        except pp.powerflow.LoadflowNotConverged as e:
            self.opf_failure_counter += 1
            logger.warning(e)
            logger.warning(f'Step {self.current_step}: Load flow failed -> complete failure of OPF.')
            raise pp.powerflow.LoadflowNotConverged
        except pp.optimal_powerflow.OPFNotConverged as e:
            self.opf_failure_counter += 1
            logger.warning(e)
            logger.warning(f'Step {self.current_step}: OPF failed. Check if old results are valid.')
            net.sgen.q_mvar = self.last_q_values / net.sgen.scaling
            self._ensure_reactive_constraints(net)
            pp.runpp(net)
            if self._constraints_satisfied(net):
                logger.warning('Results from last step still valid. Re-use reactive power values as good-enough solution (Workaround!)')
                return True

            logger.warning('Results from last step are not valid!')
            logger.warning('Relax constraints and try again.')
            if self.constraint_handler.counter >= self.n_constraint_relaxes:
                raise pp.optimal_powerflow.OPFNotConverged

            self.constraint_handler.relax_constraints(net)
            self._optimal_power_flow(net)

    def _ensure_reactive_constraints(self, net):
        # TODO: move to other file!
        net.sgen.q_mvar = net.sgen[['q_mvar', 'max_q_mvar']].min(axis=1)
        net.sgen.q_mvar = net.sgen[['q_mvar', 'min_q_mvar']].max(axis=1)

    def _constraints_satisfied(self, net):
        """ Check if all system constraints are satisfied currently. """
        # TODO: move to other file!
        overvoltage = sum(net.res_bus.vm_pu > net.bus.max_vm_pu)
        undervoltage = sum(net.res_bus.vm_pu < net.bus.min_vm_pu)
        line_overload = sum(net.res_line.loading_percent > net.line.max_loading_percent)
        trafo_overload = sum(net.res_trafo.loading_percent > net.trafo.max_loading_percent)
        return not bool(overvoltage + undervoltage + line_overload + trafo_overload)

    def _create_stack_of_observations(self, net):
        """ Get current observation and stack it to past observations. """
        obs = self._get_single_observation(net)
        if self.n_past_obs == 1 or self.obs is None:
            self.obs = obs
        elif len(self.obs.shape) == 1:
            self.obs = np.vstack((self.obs, obs))
        else:
            self.obs = np.vstack((self.obs[-self.n_past_obs+1:], obs))

    def _get_single_observation(self, net):
        """ Return observed part of network of current time step. """
        # TODO: re-evaluate that whole observation getting!!! Too complicated
        # Instead define a lambda function for each observation?!
        # -> [lambda1, lambda2...] -> iterate over all functions
        obs = self._get_network_observation(net)
        # Current time cannot be accessed from the power network
        obs += self._get_time_observation()
        # Next power values are accessed from the profiles
        obs += self._get_next_power_observation()
        self.current_obs = np.array(obs)
        assert not np.isnan(self.current_obs).any()
        if self._autoscale_obs:
            return self._autoscaler(self.current_obs)
        return self.current_obs

    def _get_network_observation(self, net) -> list:
        return [net[key[0]][key[1]][key[2]]
               for key in self.observation_space_keys
               if (isinstance(key, tuple) and isinstance(key[0], str))]

    def _get_time_observation(self):
        """ Return current time in sinus/cosinus form.
        Example daytime: (0.0, 1.0) = 00:00 and (1.0, 0.0) = 06:00.
        Idea from https://ianlondon.github.io/blog/encoding-cyclical-features-24hour-time/ """
        dayly, weekly, yearly = (24*4, 7*24*4, self.n_steps)
        time_obs = []
        for timeframe in (dayly, weekly, yearly):
            timestep = self.current_step % timeframe
            cyclical_time = 2 * np.pi * timestep / timeframe
            time_obs.append(np.sin(cyclical_time))
            time_obs.append(np.cos(cyclical_time))

        return time_obs

    def _get_next_power_observation(self):
        return [self.profiles[key[0]][key[1]][self.current_step+key[2]]
                for key in self.observation_space_keys
                if (isinstance(key, tuple) and isinstance(key[0], tuple))]

    def reset(self, start_step='random'):
        """ Reset the environment to random or specific step.
        **start_step**
        'random': Jump to some random step.
        'full': Start to step 0 and run full year (e.g. for testing).
        int: Set to specific step (mainly for dev and reproducing results).
        """
        logger.debug('Reset episode')
        self.done = False
        self.last_q_values = None
        if isinstance(start_step, int):
            self._to_specific_step(start_step)
        elif isinstance(start_step, str) and start_step=='random':
            self._to_random_step()
        elif isinstance(start_step, str) and start_step=='full':
            self._to_start_step()

        self.set_storage_levels(self.net)

        try:
            self.obs = self._create_n_past_observations()
        except ResetFailure:
            return self._rereset(start_step)

        return self.obs.flatten()

    def _rereset(self, start_step):
        """ Try other random time steps recursively. """
        assert start_step == 'random'
        logger.error('Reset went wrong. Try again with another random step.')
        return self.reset(start_step='random')

    def test_reset(self, test_episode: int):
        """ Reset environment to first step of a test episode. """
        # TODO: Make sure that inital storages are not set randomly! Reproduceablity!
        self._to_test_step(episode=test_episode)
        return self.reset(start_step=None)

    def _to_specific_step(self, to_step: int):
        self.current_step = to_step
        self.last_step = to_step + self.train_epis_length

    def _to_start_step(self):
        """ Set power network to the first step of the timeseries. Results
        in a full run through the whole timeseries, e.g. for testing.
        Warning: tests on all (!) the data, also on the training data!
        Use '_to_test_step()' instead to set to some test episode. """
        self.current_step = -1
        self.last_step = self.n_steps

    def _to_random_step(self):
        """ Set power network to random time step and therefore state.
        Uses only steps from a whitelist that excludes test steps. """
        rs = random.choice(self.whitelist)
        self.current_step = rs
        self.last_step = rs + self.train_epis_length + self.n_past_obs

    def _to_test_step(self, episode: int):
        """ Set power network to first step of a test episode. """
        self.current_step = self.test_episodes[episode][0]
        self.last_step = self.test_episodes[episode][-1]

    def _create_n_past_observations(self):
        """ If we reset our env, we need some past observations for our agent. Create multiple past observations by filling the storages
        randomly and performing random actions. Overflowing is not an issue
        here since reward is thrown away anyway. """
        self.obs = None
        for _ in range(self.n_past_obs):
            logger.info('Run step() to create stack of past observations')
            _, _, done, _ = self.step(actions='baseline')
            if done:
                raise ResetFailure('Env must not be done this early')
        return self.obs

    def _random_actions(self):
        """ Sample some random actions from the action space. """
        return self.action_space.sample()

    def _baseline_actions(self):
        """ Act in a way a non-attacker would act. Maximize generator active
        power and do not manipulate the loads in any way (storage level
        stays constant = do not use loads as shiftable loads). """
        df = self.profiles[('load', 'p_mw')] * self.controllable_share
        loss_mw = df.loc[self.current_step, self.attacking_loads]
        loss_pu = loss_mw / self.net.load['storage_power_p_mw']
        # Choose load factor so that loss gets compensated exactly
        actions = np.array(loss_pu[self.attacking_loads])
        if not self.load_actuators_only:
            # Set generation factor to one for all gens
            actions = np.append(actions, np.ones(len(self.attacking_sgens)))
        return actions

    def _set_storage_levels_random(self, net):
        """ Set the current storage level of every load to some random value
        in the range [0%, 100%]. """
        logger.debug('Set random storage levels')
        net.load['storage_level_pu'] = np.random.random(len(net.load.index))

    def _set_storage_levels_to_1(self, net):
        """ Set the current storage level of every load to some random value
        in the range [0%, 100%]. """
        logger.debug('Set storage levels all to 1')
        net.load['storage_level_pu'] = np.ones(len(net.load.index))

    def _autoscaler(self, obs):
        """ Scale single observation to [0...1]. """
        return np.divide((obs - self.low), (self.high - self.low))

    def seed(self, value):
        self._seed = value
        np.random.seed(seed=value)
        random.seed(value)

    def extract(self, state):  # Wrapper needed by Rainy.
        return state

    def step_and_render(self, action, render):
        """ Needed by rainy """
        return self.step(action)

    def close(self):
        return True

if __name__ == '__main__':
    # Run the environment with simple random agent. """
    env = QMarketEnv(grid_id='1-MV-comm--0-no_sw',
             attacking_loads=range(7,13),
             attacking_sgens=range(6,12),
             prevent_overflow=True,
             prevent_underflow=True,
             autoscale_obs=False,
             log_level=logging.WARNING)
    action_space = env.action_space
    obs = env.reset(start_step=1000)
    for stp in range(5):
        print('observation:')
        pprint(list(zip(env.observation_space_keys, obs)))
        actions = env._baseline_actions()
        print('Baseline actions: ', actions)
        obs, reward, done, _ = env.step(actions=actions)
        print('reward: ', reward)
        print('metrics: ')
        pprint(env.get_health_metrics())
        print()
        if done:
            print('Reset environment')
            env.reset()
